# Café Guix 20221025

## Résumé

Si le monde des environnements logiciels vous paraît difficile d’accès mais que vous voulez mener vos expérimentations
numériques dans un cadre le plus reproductible possible, cette session est pour vous.
Nous découvrirons le gestionnaire d’environnements logiciels Guix, par rapport à ce que vous pouvez connaître, puis nous verrons,
très concrètement, comment l’utiliser en quelques commandes, se comptant sur les doigts d’une main.

## Ressources

Les slides sont dispos à cette adresse : https://gricad_formations.gricad-pages.univ-grenoble-alpes.fr/cafe-guix-20221025/

Le PDF, par ici : https://gricad_formations.gricad-pages.univ-grenoble-alpes.fr/cafe-guix-20221025/index.pdf

Vous trouverez dans ce dépôt, des exemples de fichiers `channels.scm` et `manifest.scm`. 

